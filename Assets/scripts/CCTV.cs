﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCTV : MonoBehaviour {

    public GameObject player;
    public GameObject chaser;

    public float speed;
    public float distance;
    public float scale;

    public Vector3 tvOffset;
	
	void Start () {
		
	}
	
	
	void Update () {

        float enemyDistance = Vector3.Distance(player.transform.position, chaser.transform.position);

        if (enemyDistance > 2 && enemyDistance < 15)
        {
            scale = enemyDistance;
        }        

        if (scale > 2)
        {
            GetComponent<Camera>().orthographicSize = scale;

            tvOffset = new Vector3(scale, -3 + scale, 0);
        }else
        {
            tvOffset = Vector3.zero;
        }

    }

    private void FixedUpdate()
    {
        Vector3 followPosition = player.transform.position;

        followPosition.z = distance;

        followPosition += tvOffset;

        transform.position = Vector3.Lerp(transform.position, followPosition, speed);
    }
}
