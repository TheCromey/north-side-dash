﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {

    public bool destroyPls;
    MeshRenderer mesh;

    GameManager manager;

	void Start () {

        mesh = GetComponent<MeshRenderer>();

	}
	
	
	void Update () {
		
        if (destroyPls == true)
        {
            mesh.enabled = false;
        }

	}

    ////This is going to add points to the player's wallet.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GameManager.points ++;

            PlayerInventory.Instance.points++;

            Destroy(gameObject);
            //destroyPls = true;
        }
    }
}
