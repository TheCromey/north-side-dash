﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menus : MonoBehaviour {

    public static bool isPaused;

    public GameObject PauseUI;
    public GameObject SettingsUI;
    public GameObject CreditsUI;
    public GameObject SceneSelectionUI;
    public GameObject SceneSelection_02UI;
    public GameObject ContinueUI;

    void Update () {
        	
	}

    public void Resume()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void Pause()
    {
        Debug.Log("Game is Paused");
        PauseUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void Settings()
    {
        SettingsUI.SetActive(true);
    }

    public void Credits()
    {
        CreditsUI.SetActive(true);
    }
    public void ResumeMenu()
    {
        SettingsUI.SetActive(false);
        CreditsUI.SetActive(false);
    }

    public void SceneSelection()
    {
        SceneSelectionUI.SetActive(true);
    }
    public void SceneSelection_01_BAck()
    {
        SceneSelectionUI.SetActive(false);
    }
    public void SceneSelection_02_BAck()
    {
        SceneSelection_02UI.SetActive(false);
    }
    public void SceneSelection2()
    {
        SceneSelection_02UI.SetActive(true);
    }
    public void Continue()
    {
        ContinueUI.SetActive(true);
    }
    public void ContinueOFF()
    {
        ContinueUI.SetActive(false);
    }

}
