﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {

    AudioSource audioS;

    public static float volume = 1f;

	void Start () {

        audioS = GetComponent<AudioSource>();

    }
	
	
	void Update () {

        audioS.volume = volume;

	}
    public void SetVolume(float vol)
    {
        volume = vol;
    }

}
