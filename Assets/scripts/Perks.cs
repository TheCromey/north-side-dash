﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perks : MonoBehaviour {

     GameObject speedBoost;
     GameObject jumpBoost;

    public bool isDestroyed;

     MeshRenderer mesh;

	void Start () {
        speedBoost = GetComponent<GameObject>();
        jumpBoost = GetComponent<GameObject>();
        mesh = GetComponent<MeshRenderer>();
	}

	void Update () {
		
        if (isDestroyed == true)
        {
            mesh.enabled = false;
        }

	}
    //This is going to destroy the perk.
    private void OnTriggerEnter2D(Collider2D collision)
    { 
        isDestroyed = true;

    }
}
