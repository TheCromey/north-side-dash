﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

    private static PlayerInventory instance;
    public static PlayerInventory Instance
    {
        get
        {
            return instance ?? (instance = new GameObject("PlayerInventory").AddComponent<PlayerInventory>());

        }
    }



    public int points;

    public bool hasSunHat;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

}
