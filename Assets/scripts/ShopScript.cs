﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour {

    //  GameManager manager;
    Player player;

    public GameObject sunUI;
	
	void Start () {

        //  manager = GetComponent<GameManager>();
        player = GetComponent<Player>();
	}
	
	
	void Update () {
		
	}
    //This will purchase the Sun if the player has enough money and disable the panel.
    public void PurchaseSun()
    {
        if (PlayerInventory.Instance.points >= 10)
        {
            sunUI.SetActive(false);
            Debug.Log("Player has bought the Sun!");
            PlayerInventory.Instance.hasSunHat = true;
            PlayerInventory.Instance.points -= 10;
        }
        else
        {
            Debug.Log("Player does not have enough Money!");
        }
    }
}
